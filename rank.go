package seafoam

type NodeRank uint8

const (
	// Rank Normal indicates nodes that have no rank besides their default.
	RankNormal NodeRank = iota

	// Rank Leader indicates the node who is in charge of the cluster.
	RankLeader

	// Rank Lighthouse indicates a node who MUST be included in the cluster.
	RankLighthouse
)

func (r NodeRank) String() string {
	switch r {
	case RankNormal:
		return "NORMAL"
	case RankLeader:
		return "LEADER"
	case RankLighthouse:
		return "LIGHTHOUSE"
	default:
		return "UNDEFINED"
	}
}
