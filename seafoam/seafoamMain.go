/*
Copyright 2016 The Smudge Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"gitlab.com/DMDavis/seafoam"
	"net"
)

type MyStatusListener struct {
	seafoam.StatusListener
}

func (m MyStatusListener) OnChange(node *seafoam.Node, status seafoam.NodeStatus) {
	fmt.Printf("Node %s is now status %s\n", node.Address(), status)
}

type MyBroadcastListener struct {
	seafoam.BroadcastListener
}

func (m MyBroadcastListener) OnBroadcast(b *seafoam.Broadcast) {
	fmt.Printf("Received broadcast from %s: %s\n",
		b.Origin().Address(),
		string(b.Bytes()))
}

func main() {
	heartbeatMillis := 500
	listenPort := 9999

	// Set configuration options
	seafoam.SetListenPort(listenPort)
	seafoam.SetHeartbeatMillis(heartbeatMillis)
	seafoam.SetListenIP(net.ParseIP("127.0.0.1"))

	// Add the status listener
	seafoam.AddStatusListener(MyStatusListener{})

	// Add the broadcast listener
	seafoam.AddBroadcastListener(MyBroadcastListener{})

	// Add a new remote node. Currently, to join an existing cluster you must
	// add at least one of its healthy member nodes.
	node, err := seafoam.CreateNodeByAddress("localhost:10000")
	if err == nil {
		seafoam.AddNode(node)
	}

	// Start the server!
	seafoam.Begin()
}
