# Seafoam

### NOTE: Serious bugs may be present in this repository.

[![GoDoc](https://godoc.org/gitlab.com/DMDavis/seafoam?status.svg)](https://godoc.org/gitlab.com/DMDavis/seafoam)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/DMDavis/seafoam)](https://goreportcard.com/report/gitlab.com/DMDavis/seafoam)

## Introduction
Seafoam is a fork of [Smudge](https://github.com/clockworksoul/smudge) that extends functionality to include leadership, datacenter, and cluster-validity awareness. Seafoam is meant to be a lightweight clustering tool that enables highly scalable and robust platform creation while still maintaining smudge's lightweight nature.

Seafoam utilizes a simple series of polling-election mechanics to prevent split-brain behaviour in largescale applications when under significant load-stress. A side effect of this is that Seafoam requires more setup than smudge, although it can be set to 'fallback mode' in which functionality is essentially identical to that of smudge.

Complete documentation is available from [the associated Godoc](https://godoc.org/gitlab.com/DMDavis/seafoam).


## Features
* Smudge-backed gossip (i.e., epidemic) protocol for dissemination, the latency of which grows logarithmically with the number of members.
* Supports both IPv4 and IPv6.
* Pluggable logging
* Leader-awareness in large clusters
* Lighthouse-based validity checking
* Low network bandwidth requirements

## How to use
To use the code, you simply specify a few configuration options (or use the defaults), create and add a node status change listener, and call the `seafoam.Begin()` function.


### Configuring the node with environment variables
Perhaps the simplest way of directing the behavior of the SWIM driver is by setting the appropriate system environment variables, which is useful when making use of Smudge inside of a container.

The following variables and their default values are as follows:

```
Variable                           | Default         | Description
---------------------------------- | --------------- | -------------------------------
SEAFOAM_CLUSTER_NAME               |     seafoam     | Cluster name for for multicast discovery
SEAFOAM_HEARTBEAT_MILLIS           |       250       | Milliseconds between heartbeats
SEAFOAM_INITIAL_HOSTS              |                 | Comma-delimmited list of known members as IP or IP:PORT
SEAFOAM_LISTEN_PORT                |       9999      | UDP port to listen on
SEAFOAM_LISTEN_IP                  |    127.0.0.1    | IP address to listen on
SEAFOAM_MAX_BROADCAST_BYTES        |       256       | Maximum byte length of broadcast payloads
SEAFOAM_MULTICAST_ENABLED          |       true      | Multicast announce on startup; listen for multicast announcements
SEAFOAM_MULTICAST_ANNOUNCE_INTERVAL|        0        | Seconds between multicast announcements, 0 will disable subsequent anouncements
SEAFOAM_MULTICAST_ADDRESS          | See description | The multicast broadcast address. Default: `224.0.0.0` (IPv4) or `[ff02::1]` (IPv6)
SEAFOAM_MULTICAST_PORT             |       9998      | The multicast listen port
```
