/*
Copyright 2016 The Smudge Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package seafoam

import (
	"errors"
	"hash/adler32"
	"net"
)

// Message contents
// ---[ Base message (13 bytes) ]---
// Bytes 00-03 Checksum (32-bit)
// Bytes 04    Verb (one of {PING|ACK|PINGREQ|NFPING})
// Bytes 05    Sender rank byte
// Bytes 06	   Sender Datacenter ID
// Bytes 07-08 Sender response port
// Bytes 09-12 Sender current heartbeat
// ---[ Per member (43 bytes) ]---
// Bytes 00    Member status byte
// Bytes 01	   Member rank byte
// Bytes 02	   Member Datacenter ID
// Bytes 03-06 Member current heartbeat
// Bytes 07-08 Member host response port
// Bytes 09-24 Member host IP (09-12 for IPv4)
// Bytes 25-26 Source host response port (13-14 for IPv4)
// Bytes 27-42 Source host IP (15-18 for IPv4)
// ---[ Per broadcast (1 allowed) (23+N bytes) ]---
// Bytes 00-15 Origin IP (00-03 for IPv4)
// Bytes 16-17 Origin response port (04-05 for IPv4)
// Bytes 18-21 Origin broadcast counter (06-09 for IPv4)
// Bytes 22-23 Payload length (bytes) (10-11 for IPv4)
// Bytes 24-NN Payload (12-NN for IPv4)

type message struct {
	sender          *Node
	senderHeartbeat uint32
	verb            messageVerb
	members         []*messageMember
	broadcast       *Broadcast
	rank            NodeRank
	datacenter      uint8
}

// Represents a "member" of a message; i.e., a node that the sender knows
// about, about which it wishes to notify the downstream recipient.
type messageMember struct {
	// The source of the gossip about the member.
	source *Node

	// The last known heartbeat of node.
	heartbeat uint32

	// The subject of the gossip.
	node *Node

	// The status that the gossip is conveying.
	status NodeStatus

	// The rank of the subject node
	rank NodeRank

	// The datacenter of the subject node
	datacenter uint8
}

// Convenience function. Creates a new message instance.
func newMessage(verb messageVerb, sender *Node, senderHeartbeat uint32, rank NodeRank, datacenter uint8) message {
	return message{
		sender:          sender,
		senderHeartbeat: senderHeartbeat,
		verb:            verb,
		datacenter:      datacenter,
		rank:            rank,
	}
}

// Adds a broadcast to this message. Only one broadcast is allowed; subsequent
// calls will replace an existing broadcast.
func (m *message) addBroadcast(broadcast *Broadcast) {
	m.broadcast = broadcast
}

// Adds a member status update to this message. The maximum number of allowed
// members is 2^6 - 1 = 63, though it is incredibly unlikely that this maximum
// will be reached without an absurdly high lambda. There aren't yet many
// 88 billion node clusters (assuming lambda of 2.5).
func (m *message) addMember(node *Node, status NodeStatus, heartbeat uint32, gossipSource *Node) error {
	if m.members == nil {
		m.members = make([]*messageMember, 0, 32)
	} else if len(m.members) >= 63 {
		return errors.New("member list overflow")
	}

	messageMember := messageMember{
		heartbeat: heartbeat,
		node:      node,
		status:    status,
		source:    gossipSource,
	}

	m.members = append(m.members, &messageMember)

	return nil
}

func (m *message) encode() []byte {
	// Pre-calculate the message size. Each message prefix is 12 bytes.
	// Each member has a constant size of 10 bytes, plus 2 times the length of
	// the IP (4 for IPv4, 16 for IPv6).
	size := 13 + (len(m.members) * (11 + ipLen + ipLen))

	if m.broadcast != nil {
		size += 8 + ipLen + len(m.broadcast.bytes)
	}

	bytes := make([]byte, size, size)

	// An index pointer (start at 4 to accommodate checksum)
	p := 4

	// Byte 04
	// Rightmost 2 bits: verb (one of {P|A|F|N})
	// Leftmost 6 bits: number of members in payload
	verbByte := byte(len(m.members))
	verbByte = (verbByte << 2) | byte(m.verb)
	p += encodeByte(verbByte, bytes, p)
	// Byte 05
	// rank
	p += encodeByte(byte(m.sender.rank), bytes, p)
	// Byte 06
	// Datacenter ID
	p += encodeByte(m.sender.dataCenter, bytes, p)
	// Bytes 07-08
	// Sender port
	p += encodeUint16(m.sender.port, bytes, p)
	// Bytes 09-12
	// Heartbeat
	p += encodeUint32(m.senderHeartbeat, bytes, p)

	// Each member data requires 25 bytes (11 for IPv4).
	for _, member := range m.members {
		snode := member.source

		// Byte 00
		// Member Status
		p += encodeByte(byte(member.status), bytes, p)
		// Byte 01
		// Member Rank
		p += encodeByte(byte(member.node.rank), bytes, p)
		// Byte 02
		// Member Datacenter
		p += encodeByte(byte(member.node.dataCenter), bytes, p)
		// Bytes 03-06
		// Member Heartbeat
		p += encodeUint32(member.heartbeat, bytes, p)
		// Bytes 07-08
		// Member Response Port
		p += encodeUint16(member.node.port, bytes, p)

		// Bytes 09-24 (09-12 in IPv4)
		// Member host IP

		var ipb net.IP

		if ipLen == net.IPv4len {
			ipb = member.node.ip.To4()
		} else if ipLen == net.IPv6len {
			ipb = member.node.ip.To16()
		}

		for i := 0; i < ipLen; i++ {
			bytes[p+i] = ipb[i]
		}

		p += ipLen

		if snode != nil {
			// Bytes 25-26 (13-14 in IPv4)
			// Source host IP
			p += encodeUint16(snode.port, bytes, p)

			//Bytes 27-42 (15-18 in IPv4)
			if ipLen == net.IPv4len {
				ipb = snode.ip.To4()
			} else if ipLen == net.IPv6len {
				ipb = snode.ip.To16()
			}

			for i := 0; i < ipLen; i++ {
				bytes[p+i] = ipb[i]
			}

			p += ipLen
		} else {
			//blanked
			p += ipLen + 2
		}
	}

	if m.broadcast != nil {
		bbytes := m.broadcast.encode()
		for i, v := range bbytes {
			bytes[p+i] = v
		}
	}

	checksum := adler32.Checksum(bytes[4:])
	encodeUint32(checksum, bytes, 0)

	return bytes
}

// If members exist on this message, and that message has the "forward to"
// status, this function returns it; otherwise it returns nil.
func (m *message) getForwardTo() *messageMember {
	if len(m.members) > 0 && m.members[0].status == StatusForwardTo {
		return m.members[0]
	}

	return nil
}

// Parses the bytes received in a UDP message.
// If the address:port from the message can't be associated with a known
// (live) node, then an instance of message.sender will be created from
// available data but not explicitly added to the known nodes.

func decodeMessage(sourceIP net.IP, bytes []byte) (message, error) {
	var err error

	// An index pointer
	p := 0

	// Bytes 00-03 Checksum (32-bit)
	checksumStated, p := decodeUint32(bytes, p)
	checksumCalculated := adler32.Checksum(bytes[4:])
	if checksumCalculated != checksumStated {
		return newMessage(255, nil, 0, 0, 0),
			errors.New("checksum failure from " + sourceIP.String())
	}

	// Byte 04
	// Rightmost 2 bits: verb (one of {P|A|F|N})
	// Leftmost 6 bits: number of members in payload
	v, p := decodeByte(bytes, p)
	verb := messageVerb(v & 0x03)

	memberCount := int(v >> 2)

	//Byte 05
	senderRank, p := decodeByte(bytes, p)

	//Byte 06
	senderDatacenter, p := decodeByte(bytes, p)

	// Bytes 07-08 Sender response port
	senderPort, p := decodeUint16(bytes, p)

	// Bytes 09-12 Sender ID Code
	senderHeartbeat, p := decodeUint32(bytes, p)

	// Now that we have the IP and port, we can find the Node.
	sender := knownNodes.getByIP(sourceIP, senderPort)

	// We don't know this node, so create a new one!
	if sender == nil {
		sender, _ = CreateNodeByIP(sourceIP, senderPort)
	}

	// Now that we have the verb, node, and code, we can build the message
	m := newMessage(verb, sender, senderHeartbeat, NodeRank(senderRank), senderDatacenter)

	memberLastIndex := p + (memberCount * (11 + ipLen + ipLen))

	if len(bytes) > p {
		m.members = decodeMembers(memberCount, bytes[p:memberLastIndex])
	}

	if len(bytes) > memberLastIndex {
		m.broadcast, err = decodeBroadcast(bytes[memberLastIndex:])
	}

	return m, err
}

func decodeMembers(memberCount int, bytes []byte) []*messageMember {

	members := make([]*messageMember, 0, 1)

	// An index pointer
	point := 0

	for point < len(bytes) {
		var memberNode *Node

		var sourceNode *Node

		// Byte 00 Member status byte
		status, p := decodeByte(bytes, point)

		// Byte 01 Member Rank byte
		rank, p := decodeByte(bytes, p)

		// Byte 02 Member Datacenter ID
		datacenter, p := decodeByte(bytes, p)

		// Bytes 03-06 member heartbeat
		heartbeat, p := decodeUint32(bytes, p)

		// Bytes 07-08 member response port
		port, p := decodeUint16(bytes, p)

		var memberIP net.IP

		if ipLen == net.IPv6len {
			// Bytes 09-24 member IP
			memberIP = make(net.IP, net.IPv6len)
			copy(memberIP, bytes[p:p+16])
		} else {
			// Bytes 09-12 member IPv4
			memberIP = net.IPv4(bytes[p+0], bytes[p+1], bytes[p+2], bytes[p+3])
		}

		p += ipLen

		if len(memberIP) > 0 {
			// Find the sender by the address associated with the message
			memberNode = knownNodes.getByIP(memberIP, port)

			// We still don't know this node, so create a new one!
			if memberNode == nil {
				memberNode, _ = CreateNodeByIP(memberIP, port)
			}
		}

		// Bytes 25-26 member response port (13-14 IPv4)
		senderPort, p := decodeUint16(bytes, p)

		var senderIP net.IP

		if ipLen == net.IPv6len {
			// Bytes 27-42 member IP
			senderIP = make(net.IP, net.IPv6len)
			copy(senderIP, bytes[p:p+16])
		} else {
			// Bytes 15-18 member IPv4
			senderIP = net.IPv4(bytes[p+0], bytes[p+1], bytes[p+2], bytes[p+3])
		}
		p += ipLen

		if len(senderIP) > 0 {
			// Find the sender by the address associated with the message
			sourceNode = knownNodes.getByIP(senderIP, senderPort)

			// We still don't know this node, so create a new one!
			if sourceNode == nil {
				sourceNode, _ = CreateNodeByIP(senderIP, senderPort)
			}
		}

		member := messageMember{
			heartbeat:  heartbeat,
			node:       memberNode,
			source:     sourceNode,
			status:     NodeStatus(status),
			rank:       NodeRank(rank),
			datacenter: datacenter,
		}

		members = append(members, &member)
		point = p
	}

	return members
}
